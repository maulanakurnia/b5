# Assigment B5 - Intern Privy

Author : Maulana Kurnia Fiqih Ainul Yaqin

Position : Intern Backend

## Endpoint

|  Method | Endpoint  |  Description |
|---------|-----------|--------------|  
|  GET    | `/external/generate/token`  |        get token   |

## Header Request

`X-Api-Key-ID`  : `<<YOUR_API_ID>>`

`Timestamp`     : `<<TIMESTAMP>>`

`Signature`     : `<<YOUR_SIGNATURE>>`

## Screenshot

### Generate Token

![Generate Token](screenshot/generate-token.png)

### Decode JWT

![Decode JWT](screenshot/decode-jwt.png)
